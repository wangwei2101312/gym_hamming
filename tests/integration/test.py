#!python2
"""
  Integration test to calculate Hamming distance between 2 ASCII strings
  Interpreter: Python 2.7
  Hints: Reads the test_cases.json table, passes the input strings to the hamming command line application
       and verifies whether the returned value is as expected

  Author: Gyula Matyas
  Version: 1.0.1
  Last modified: 2016.11.14.
"""

import os
import time
import sys
import json
import subprocess
import struct


# /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HammingTest:

    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    def __init__(self):
        """
        Constructor
        - Initialize counters, settings
        """
        # Counters for executed test cases and failures
        self._test_count = 0;
        self._fail_count = 0;
        # Ticks for test case execution: 100 mili-seconds
        self._tick = 0.1
        # Timeout for one test case: 2 seconds
        self._timeout = 2

    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    def __parse_input(self):
        """
        Reads input data from JSON file
        """
        # Scan input file of integration test cases
        json_file = open("test_cases.json").read()
        json_data = json.loads(json_file)
        self._content = json_data["input"]
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    def __execute_test(self, row):
        """
        Executes one test case
        """
        # First string
        first = str(row["first"])
        # Second string
        second = str(row["second"])
        # Expected result
        result = str(row["result"])
        # Create call command
        test_call = [self._executable, 
                     first, 
                     second]
        try:
            # Allocate child process
            test_instance = subprocess.Popen(test_call)
            sec = 0
            # Check the child process for termination
            test_instance.poll()
            while test_instance.returncode is None:
                if sec > self._timeout:
                    # When time out - stop the child process
                    self.log_error("Time out occurred by Hamming distance calculator!")
                    test_instance.kill()
                    time.sleep(self._tick)
                    break
                # Wait for a tick
                time.sleep(self._tick)
                sec += self._tick
                # Check the child process for termination
                test_instance.poll()
            # Get the returned value from the Hamming distance calculator    
            ret_val = int(test_instance.returncode)
            # Avoid unsigned int transformation of -1 returned by Hamming distance calculator
            if int(result) == -1:
                # -1 is returned as unsigned int
                if (ret_val & 0xff) == 0xff:
                    ret_val = -1 
            if str(ret_val) != result:
                # Test case failed
                print("Test case: [" + first+ "] [" + second + "] failed! " + result + " != " + str(ret_val))
                return 1
            # Test case succeeed    
            return 0    
        except Exception as e:
            print("Unexpected error: " + str(e))
            return -1
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    def __loop_tests(self):
        """
        Loop test cases
        """
        print("Performing integration test for Hamming distance calculator...")
        self.__parse_input()
        for row in self._content:
            ret_val = self.__execute_test(row)
            if ret_val == -1:
                # Unexpected error, quit the test
                return -1 
            elif ret_val == 1:
                # Test case failed
                self._fail_count += 1
            # Increment counter of executed test cases
            self._test_count += 1
        print("Test cases: " + str(self._test_count))     
        print("Succeed:    " + str(self._test_count - self._fail_count))     
        print("Failed:     " + str(self._fail_count))
        return self._fail_count
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    def run(self):
        """
        Entry point
        """
        # Get the full qualified path to Hamming calculator    
        if len(sys.argv) < 2:
            print("ERROR: No executable specified for Hamming distance calculation!")
            return -1
        self._executable = sys.argv[1]
        # Check if executable exists
        if not os.path.isfile(self._executable):
            print("ERROR: The specified executable path doesn't exist: " + self._executable)
            return -1
        return self.__loop_tests()
    # /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

    
# /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def main():    
    try:
        # Return number of failed tests
        exit(HammingTest().run())
    except Exception as e:
        # Test couldn't run - exit with error code: -1
        print(str(e))
        exit(-1)

# /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
main()        
