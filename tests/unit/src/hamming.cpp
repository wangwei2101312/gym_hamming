///////////////////////////////////////////////////////////////////////////////////////////////////
// Unit test module for Hamming distance calculator library
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2016.11.14.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <hamming.h>
#include <vector>
#include <string>
#include <list>


namespace {
    // Array of integers
    const int c_n1[] = {1,2,3,4,5};
    const int c_n2[] = {0,2,3,1,5};
}



///////////////////////////////////////////////////////////////////////////////////////////////////
/** \brief Testing Hamming distance calculation
  *
  */
TEST(hamminglib, unit_test) {
    
    {// 1. Test 2 vectors with integers
        std::vector<int> v1(c_n1, c_n1 + (sizeof(c_n1) / sizeof(int)) );
        std::vector<int> v2(c_n2, c_n2 + (sizeof(c_n2) / sizeof(int)) );
        ASSERT_EQ(2, hamming::getDistance(v1, v2));
        ASSERT_EQ(0, hamming::getDistance(v1, v1));
        ASSERT_EQ(0, hamming::getDistance(v2, v2));
    }
    {// 2. Test 2 vectors with integers
        std::vector<int> v1(c_n1, c_n1 + (sizeof(c_n1) / sizeof(int)) );
        std::vector<int> v2(c_n1, c_n1 + (sizeof(c_n1) / sizeof(int)) );
        ASSERT_EQ(0, hamming::getDistance(v1, v1));
        ASSERT_EQ(0, hamming::getDistance(v1, v2));
        ASSERT_EQ(0, hamming::getDistance(v2, v2));
    }
    { // 3. Test 2 STL strings
        std::string s1("alabama");
        std::string s2("atabama");
        ASSERT_EQ(1, hamming::getDistance(s1, s2));
    }
    {// 4. Test 2 STL strings
        std::string s1("aratata");
        std::string s2("eretete");
        ASSERT_EQ(0, hamming::getDistance(s1, s1));
        ASSERT_EQ(4, hamming::getDistance(s1, s2));
        ASSERT_EQ(0, hamming::getDistance(s2, s2));
    }
    {// 5. Test 2 STL lists
        std::list<int> l1(c_n1, c_n1 + (sizeof(c_n1) / sizeof(int)) );
        std::list<int> l2(c_n2, c_n2 + (sizeof(c_n2) / sizeof(int)) );
        ASSERT_EQ(0, hamming::getDistance(l2, l2));
        ASSERT_EQ(2, hamming::getDistance(l1, l2));
        ASSERT_EQ(0, hamming::getDistance(l1, l1));
    }
    {// 6. Test 2 char arrays
        const char* p1 = "mytext";
        const char* p2 = "MyTeXt";
        ASSERT_EQ(0, hamming::getDistance(p1, p1));
        ASSERT_EQ(3, hamming::getDistance(p1, p2));
        ASSERT_EQ(0, hamming::getDistance(p2, p2));
    }
    {// 7. Test the built-in adapter
        hamming::Adapter<int> a(c_n1, (sizeof(c_n1) / sizeof(int)));
        hamming::Adapter<int>::const_iterator it = a.begin();
        for (int i = 0; i < (sizeof(c_n1) / sizeof(int)); ++i ) {
            ASSERT_EQ(*it, c_n1[i] );
            ++it;
        }
        ASSERT_EQ(it, a.end());
    }
    {// 8. Test integer arrays
        ASSERT_EQ(0, hamming::getDistance(c_n1, c_n1, (sizeof(c_n1) / sizeof(int))));
        ASSERT_EQ(2, hamming::getDistance(c_n1, c_n2, (sizeof(c_n1) / sizeof(int))));
        ASSERT_EQ(0, hamming::getDistance(c_n2, c_n2, (sizeof(c_n2) / sizeof(int))));
    }
    {// 9. Test integer arrays with the built-in adapter
        hamming::Adapter<int> a1(c_n1, (sizeof(c_n1) / sizeof(int)));
        hamming::Adapter<int> a2(c_n2, (sizeof(c_n2) / sizeof(int)));
        ASSERT_EQ(0, hamming::getDistance(a1, a1));
        ASSERT_EQ(2, hamming::getDistance(a1, a2));
        ASSERT_EQ(0, hamming::getDistance(a2, a2));
    }
    {// 10. Test 2 wide char arrays
        const wchar_t* p1 = L"mytext";
        const wchar_t* p2 = L"MyTeXt";
        ASSERT_EQ(0, hamming::getDistance(p1, p1));
        ASSERT_EQ(3, hamming::getDistance(p1, p2));
        ASSERT_EQ(0, hamming::getDistance(p2, p2));
    }
}
