################################################################################
#
#  build_linux.sh: Generates CMake based build, using make
#
#  - Solution is generated into the build/linux sub-folder
#  - Command line parameters: add cmake command line options, when default build
#    parameters need to be changed (see cmake -h for options)
#     Options:
#      -DCMAKE_BUILD_TYPE="Debug" , generates debug build
#      -DHAMMING_ENABLE_GTEST=ON  , enables GTest framework
#
###############################################################################
if ! [ -d build ]
then
    mkdir build
fi
if ! [ -d build/linux ]
then
    mkdir build/linux
fi
cd build/linux
cmake "$@" ../../
make
