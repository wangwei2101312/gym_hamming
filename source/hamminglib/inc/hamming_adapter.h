///////////////////////////////////////////////////////////////////////////////////////////////////
// STL container like adapter for Hamming distance calculation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2016.11.14.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HAMMING_ADAPTER_H__
#define __HAMMING_ADAPTER_H__

namespace hamming {
    
    /** \brief Built-in adapter class to transform raw pointers into STL container type objects
      *  - implements a forward iterator, named const_iterator, compatible with the STL Iterator
      *  - implements a const_iterator begin() method, which returns iterator at the first element
      *  - implements a const_iterator end() method, which returns iterator at next to the last element
      */
    template<class T> 
    class Adapter {
    public:    
        /** \brief Constant iterator class for the Hamming built-in adapter
          *  - implements a forward iterator, named const_iterator, compatible with the STL Iterator
          *  - implements a const_iterator begin() method, which returns iterator at the first element
          *  - implements a const_iterator end() method, which returns iterator at next to the last element
          */
        class const_iterator {
        public:    
            /** \brief Default constructor 
              */
            const_iterator() 
            : m_p(NULL) {
            }

            /** \brief Copy constructor 
              * \param[in] other - reference to the iterator, where from copy
              */
            const_iterator(const const_iterator& other) 
            : m_p(other.m_p) {
            }

            /** \brief Constructor 
              * \param[in] p - pointer to adapter element
              */
            const_iterator(const T* p) 
            : m_p(p) {
            }
            
        public:    
            /** \brief Dereference operator 
              * \returns - reference to the current adapter element
              */
            const T& operator *() const {
                return *m_p;
            }
                
            /** \brief Structure dereference operator 
              * \returns - pointer to the current adapter element
              */
            const T* operator ->() const {
                return m_p;
            }
            
            /** \brief Assignment operator
              * \param[in] other - reference to the iterator where from copy 
              * \returns - reference to the current instance
              */
            const_iterator& operator = (const const_iterator& other) {
                m_p = other.m_p;
                return *this;
            }
            
            /** \brief Comparison operator
              * \param[in] other - reference to the iterator to compare 
              * \returns - true, when the iterators point to the same element, otherwise false
              */
            bool operator == (const const_iterator& other) const {
                return (m_p == other.m_p);
            }

            /** \brief Non equal operator
              * \param[in] other - reference to the iterator to compare 
              * \returns - false, when the iterators point to the same element, otherwise true
              */
            bool operator != (const const_iterator& other) const {
                return !(*this == other);
            }
        
            /** \brief Increment (prefix) operator
              * \returns - the current instance, pointing to the next adapter element
              */
            const_iterator& operator ++() {
                m_p++;
                return *this;
            }
                       
        private:
            // Pointer to adapter element
            const T* m_p;
        };
        
    public:
        /** \brief Constructor 
          * \param[in] p - pointer to array
          * \param[in] len - number of elements in the array
          */
        Adapter(const T* p, size_t len) 
        : m_p(p)
        , m_len(len) {
        }
        
        /** \brief Iterator to the first element of the contained array 
          * \returns - iterator object
          */
        const_iterator begin() const {
            return const_iterator(m_p);
        }
        
        /** \brief Iterator to the last element of the contained array 
          * \returns - iterator object
          */
        const_iterator end() const {
            return const_iterator(m_p + m_len);
        }
        
    private:
        // Disabled default/copy constructors and assignment operator
        Adapter();
        Adapter(const Adapter&);
        Adapter& operator = (const Adapter&);

    private:
        // Pointer to array
        const T* m_p;
        // Elements count of array
        size_t   m_len;
    };

}//namespace hamming

#endif//__HAMMING_ADAPTER_H__
