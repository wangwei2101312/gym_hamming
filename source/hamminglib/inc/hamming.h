///////////////////////////////////////////////////////////////////////////////////////////////////
// Hamming distance calculator template functions
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2016.11.14.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HAMMING_H__
#define __HAMMING_H__

#include <iterator>
#include <cassert>
#include <hamming_adapter.h>

/**! \brief Collection of functions calculating the Hamming distance between 2 BLOBs.
  *
  *  The functions are grouped into the hamming namespace, accepting as input 2 objects 
  *  and returning the value of the Hamming distance
  *  The responsibility to ensure input objects with identical elements count, belongs to the user,
  *  the input methods are asserting only, when invalid inputs are recognized
  *  The Hamming functions accepts STL container like objects (like string, vector, list, ..), 
  *  which need to implement a constant forward iterator, identical with the STL Iterator 
  *  There exists a built-in adapter also, in order to can use raw pointers as input.
  *  There exists the possibility to implement an own adapter, similar to the built-in adapter for 
  *  handling special cases 
  *   Example: 
  *    - using as input UTF8 strings, where characters might be represented on different byte counts
  *    - using as input pictures, having different color depth
  *    - bitwise calculation mode of the Hamming distance
  */ 

namespace hamming {
    
    /** \brief Calculates Hamming distance between 2 STL like container objects.
      * The input objects need to meet the following requirements:
      *  - implement a forward iterator, named const_iterator, compatible with the STL Iterator
      *  - implement a const_iterator begin() method, which returns iterator at the first element
      *  - implement a const_iterator end() method, which returns iterator at next to the last element
      *  - the iterators must be able to dereference elements for equality comparison 
      * \param[in] first - reference to the first object
      * \param[in] second - reference to the second object
      * \returns the number of different elements on the same iterations
      */
    template<class T>
    size_t getDistance(const T& first, const T& second) {
        // Initialize the Hamming distance with zero
        size_t dist = 0;
        // Iterate over the objects, controlling the loop with first object
        typename T::const_iterator it_1 = first.begin();
        typename T::const_iterator it_2 = second.begin();
        for (; it_1 != first.end(); ++it_1, ++it_2) {
            assert(it_2 != second.end()); // ensure the second object has a valid element
            if (*it_1 != *it_2) {
                // When 
                dist++;
            }
        }
        assert(it_2 == second.end()); // ensure the second object ended also
        return dist;
    }

    
    /** \brief Calculates Hamming distance between 2 objects, using an adapter. 
      * The adapter needs to meet the following requirements:
      *  - to determine the number of elements of each object, without explicit specification
      *  - implement a forward iterator, named const_iterator, compatible with the STL Iterator
      *  - implement a const_iterator begin() method, which returns iterator at the first element
      *  - implement a const_iterator end() method, which returns iterator at next to the last element
      *  - the iterators must be able to dereference elements for equality comparison 
      * \param[in] first - reference to the first object
      * \param[in] second - reference to the second object
      * \returns the number of different elements on the same iterations
      */
    template<class T,       // user defined object type
             class A>       // adapter object type
    size_t getDistance(const T& first, const T& second) {
        // Encapsulate the objects into adapter instances and invoke the generic calculation 
        return getDistance<T>( A(first), 
                               A(second));
    }

    
    /** \brief Calculates Hamming distance between 2 objects, using an adapter and explicit 
      * specification of the elements count.
      * The adapter needs to meet the following requirements:
      *  - implement a forward iterator, named const_iterator, compatible with the STL Iterator
      *  - implement a const_iterator begin() method, which returns iterator at the first element
      *  - implement a const_iterator end() method, which returns iterator at next to the last element
      *  - the iterators must be able to dereference elements for equality comparison 
      * \param[in] first - reference to the first object
      * \param[in] second - reference to the second object
      * \returns the number of different elements on the same iterations
      */
    template<class T,       // user defined object type
             class A,       // adapter object type
             size_t len>    // number of elements
    size_t getDistance(const T& first, const T& second) {
        // Encapsulate the objects into adapter instances and invoke the generic calculation 
        return getDistance<T>( A(first, len), 
                               A(second, len));
    }
    
    
    /** \brief Calculates Hamming distance between 2 raw pointer provided arrays, using the 
      * built-in adapter and explicit specification of the elements count.
      * \param[in] first - pointer to the first array
      * \param[in] second - pointer to the second array
      * \returns the number of different elements on the same iterations
      */
    template<class T>
    size_t getDistance(const T* first, const T* second, size_t len) {
        // Encapsulate the objects into built-in adapter instances and invoke the generic 
        // calculation 
        return getDistance<Adapter<T> >( Adapter<T>(first, len), 
                                         Adapter<T>(second, len));
    }


    /** \brief Calculates Hamming distance between 2 raw pointer provided null terminated arrays, 
      * using the built-in adapter. The elements counts are determined as distance between the 
      * starting element and the first zero ('\0') element
      * \param[in] first - pointer to the first array
      * \param[in] second - pointer to the second array
      * \returns the number of different elements on the same iteration
      */
    template<class T>
    size_t getDistance(const T* first, const T* second) {
        // Determine the length of inputs, by searcing for the first zero element
        const T* last(first);
        while (*last) {
            last++;
        }
        // Encapsulate the objects into adapter instances and invoke the generic calculation 
        return getDistance<Adapter<T> >( Adapter<T>(first, (last - first)), 
                                         Adapter<T>(second, (last - first)));
    }
   

}//namespace hamming

// Include template specializations
#include <hamming_spec.h>


#endif//__HAMMING_H__
