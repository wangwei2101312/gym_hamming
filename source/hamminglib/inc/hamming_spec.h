///////////////////////////////////////////////////////////////////////////////////////////////////
// Specialized template functions for Hamming distance calculation
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2016.11.14.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HAMMING_SPEC_H__
#define __HAMMING_SPEC_H__

#include <string.h>

namespace hamming {
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Specialize for ANSI character array
    template<>
    size_t getDistance<char>(const char* first, const char* second) {
        return getDistance<Adapter<char> >(Adapter<char>(first, strlen(first)), 
            Adapter<char>(second, strlen(second)));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Specialize for wide character array
    template<>
    size_t getDistance<wchar_t>(const wchar_t* first, const wchar_t* second) {
        return getDistance<Adapter<wchar_t> >(Adapter<wchar_t>(first, wcslen(first)), 
            Adapter<wchar_t>(second, wcslen(second)));
    }

}//namespace hamming




#endif//__HAMMING_SPEC_H__
