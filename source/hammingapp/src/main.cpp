///////////////////////////////////////////////////////////////////////////////////////////////////
// GTest based unit test for Hamming distance calculator library
// 
// Author: Gyula Matyas
// Version: 1.0.1
// Last modified: 2016.11.14.
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
#include <hamming.h> 
#include <iostream>
#include <string.h>

/** \brief Entry point into command line application, which calculates Hamming distance between 
  * 2 ASCII strings
  * \param[in] argv[1] - first user string
  * \param[in] argv[2] - second user string
  * \returns the Hamming distance, when inputs are valid, otherwise -1
*/
int main(int argc, char *argv[]) {
    size_t retval = -1;
    // Check command line arguments (1st argument to be skipped, it is system related)
    if (3 == argc) {
        // When 2 user arguments found and they have identical length
        if (strlen(argv[1]) == strlen(argv[2])) {
            // Calculate the Hamming distance between the 2 input strings
            retval = hamming::getDistance(static_cast<const char*>(argv[1]), 
                                          static_cast<const char*>(argv[2]));
            std::cout << "Hamming distance: " << retval << std::endl;
        }
        else {
            // String length is not identical - report error
            std::cout << "ERROR: Input strings have different length:" << std::endl
                      << " - " << argv[1] << " => " << strlen(argv[1]) << std::endl
                      << " - " << argv[2] << " => " << strlen(argv[2]) << std::endl;
        }
    }
    else if ( (2 == argc) &&
              ( (0 == strcmp(argv[1], "-h")) ||
                (0 == strcmp(argv[1], "--help")) ||
                (0 == strcmp(argv[1], "/?")))) {
        // One user argument found only and it matches the "help" invoking option -> display help
        std::cout << "Terminal application to calculate Hamming distance between 2 equal length ASCII strings" << std::endl;
        std::cout << "Usage:" << std::endl;
        std::cout << " -h, --help, /?      = displays command line options" << std::endl;
        std::cout << " [string1] [string2] = calculates the Hamming distance between string1 and string2" << std::endl;
    }
    else {
        // Arguments count is not correct
        std::cout << "Invalid number of arguments: expected 2 and found " << (argc - 1) << std::endl;
    }
    return int(retval);
}