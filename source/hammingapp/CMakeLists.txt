###############################################################
#
#  Hamming distance calculator command line application
#  - The application returns the Hamming distance between 2 input strings, 
#    given from the command line
#
###############################################################
# Project name
project(hammingapp)

# CMake version
cmake_minimum_required(VERSION 2.6)

# Source files
set(source_files
    src/main.cpp
)

# Specify linking directories
link_directories(${LIBRARY_OUTPUT_PATH})

# Generate executable
add_executable(hamming 
               ${source_files})

# Link Hamming library
target_link_libraries(hamming 
                      hamminglib)


