:: #############################################################################
::
::  build_windows.bat: Generates CMake based solution using the default generator
::
::  - Solution is generated into the build/windows sub-folder
::  - Command line parameters: add cmake command line options, when default generator, or 
::    default build parameters need to be changed (see cmake -h for options)
::    Example:
::    - build_windows -G "Visual Studio 10 2010" , generates solution for Visual Studio 2010
::     Options:
::      -DHAMMING_ENABLE_GTEST=ON, enables GTest framework for unit test

::
:: #############################################################################
if not exist %cd%\build mkdir %cd%\build
if not exist %cd%\build\windows mkdir %cd%\build\windows
cd %cd%\build\windows
cmake %* %cd%\..\..\